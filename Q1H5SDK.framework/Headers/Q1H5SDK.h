//
//  Q1H5SDK.h
//  Q1H5SDK
//
//  Created by geekluo on 2023/1/11.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef void (^H5ResponseCallback)(id _Nullable responseData);
typedef void (^H5Callback)(id _Nullable responseData, H5ResponseCallback _Nullable responseCallback);

NS_ASSUME_NONNULL_BEGIN

@interface Q1H5SDK : NSObject

+ (instancetype)Instance;

/**
 *  使用WKWebView打开H5小游戏链接
 *
 *  @param vc                          当前顶层控制器（UnityViewController）
 *  @param urlString           小游戏url链接或本地资源入口路径(index.html)
 *  @param inBackground    是否在后台打开
 *  @param callback             JS调用OC的方法和回调
 */
- (void)openH5GamesWithVC:(UIViewController *)vc
             inBackGround:(BOOL)inBackground
                URLString:(NSString *)urlString
               jsCallback:(H5Callback)callback;

/**
 * H5 Webview是否展示
 */
- (BOOL)isH5ViewShow;

/**
 * H5 Webview是否已打开
 */
- (BOOL)isH5ViewOpen;

/**
 * 显示H5 Webview
 * 已经在后台打开的情况下调用来展示
 */
- (void)showH5VC;

/**
 *  通过JS交互传给小游戏调用的方法和参数  SDK调用JS
 *
 *  必须先调用openH5GamesWithVC后才生效
 *  @param eventName           事件名
 *  @param msg                        事件参数，json字符串格式
 *  @param callback             H5回调回来的参数，可以为空
 */
- (void)h5GameTriggerEvent:(NSString *)eventName
                       msg:(NSString *)msg
                  callback:(nullable H5ResponseCallback)callback;

/**
 *  关闭H5game容器
 */
- (void)closeH5Game;

@end

NS_ASSUME_NONNULL_END
