Pod::Spec.new do |s|
  s.name = "Q1H5SDK"
  s.version = "1.0.1"
  s.summary = "Q1H5SDK小游戏加载容器"
  s.homepage = "https://gitee.com/geekluo007"
  s.authors = { "q1.com" => "https://gitee.com/q1com" }
  s.platform = :ios, "11.0"
  s.source = { :git => "https://gitee.com/geekluo007/q1-h5-sdk.git", :tag => "#{s.version}" }
  s.vendored_frameworks = '*.{framework}'
  s.license      = { :type => "MIT", :file => "FILE_LICENSE" }
  s.frameworks = 'Foundation', 'UIKit'
  s.dependency  'GLAWebViewJavascriptBridge','1.0.0'
  s.pod_target_xcconfig = { 'VALID_ARCHS' => 'x86_64 armv7 arm64' }
end

